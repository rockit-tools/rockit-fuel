---@alias RBXScriptConnection RobloxScriptConnection
---@alias Objects table
---@alias Tuple any
---@alias Content string
---@alias Variant any
---@alias ProtectedString string
---@alias BinaryString string
---@alias QDir string
---@alias QFont string
---@alias Map table

---@class RBXScriptSignal
RBXScriptSignal = {}

--- Connect a Script Signal
---@param f function Callback function for the signal
---@return RBXScriptConnection The resulting script connection
function RBXScriptSignal:Connect(f)

end

--- Wait for this signal to fire
---@return any What was fired to the signal
function RBXScriptSignal:Wait()

end

---@class DockWidgetPluginGuiInfo
DockWidgetPluginGuiInfo = {}

---@returns DockWidgetPluginGuiInfo
function DockWidgetPluginGuiInfo.new()
end

--- Wait n amount of seconds before continuing the thread. This function is a request to wait and will return the
---actual number of seconds waited for.
---@param n number Number of seconds to wait for
---@return number, number The actual number of seconds waited and elapsedTime
function wait(n)

end

--- Schedules a function to be executed after *delayTime* seconds have passed, without yielding the current thread. This function allows multiple Lua threads to be executed in parallel from the same stack.
---@param delayTime number
---@param callback function
function delay(delayTime, callback)

end

--- Returns how much time has elapsed since the current instance of Roblox was started. In Roblox Studio, this begins counting up from the moment Roblox Studio starts running, not just when opening a place.
---@return number
function elapsedTime()

end

--- Runs the specified callback function in a separate thread, without yielding the current thread. The function will be executed the next time Roblox's *Task Scheduler* runs an update cycle. This delay can take up to 1/30th of a second.
---@param callback function
function spawn(callback)

end

--- Returns the amount of time that has elapsed since the current game instance started running. If the current game instance is not running, this will be 0.
---@return number
function time()

end

--- Returns the amount of time that has elapsed since the current game instance started running. If the current game instance is not running, this will be 0.
---@return number
function tick()

end

--- Returns the UserSettings object, which is used to read information from the current user's game menu settings.
---@see UserSettings
---@return UserSettings
function UserSettings()

end

--- Returns the current version of Roblox as a string. The integers in the version string are separated by periods, and each integers represent the following, in order: Generation - The current generation of the application shell that is hosting the client. Version - The current release version of Roblox. Patch - The current patch number for this version of Roblox. Commit - The ID of the last internal commit that was accepted into this version of the client.
---@return string
function version()

end

--- Behaves identically to Lua's print function, except the output is styled as a warning, with yellow text and a timestamp. This function accepts any number of arguments, and will attempt to convert them into strings which will then be joined together with spaces between them.
function warn(...)
end

--- A reference to the script object that is executing the code you are writing. It can be either a Script, a LocalScript, or a ModuleScript (and sometimes a CoreScript) This variable is not available when executing code from Roblox Studio's command bar.
---@type LuaSourceContainer
script = {}

--- A reference to the DataModel, which is the root Instance of Roblox's parent/child hierarchy.
---@type DataModel
game = {}

--- A reference to the Plugin object that represents the plugin being ran from this Script. This reference only exists in the context where a script is executed as a plugin.
---@type Plugin
plugin = {}

--- A table that is shared across all scripts that share the same execution context level. This serves the exact same purpose as _G.
---@type table
shared = {}

--- A reference to the Workspace service, which contains all of the physical components of a Roblox world.
---@type Workspace
---@see Workspace
workspace = {}

--- Get the type of the input object as a string
---@param object any
---@return string
---@see type
function typeof(object)

end
