local zlib = require("zlib")

__real__io = io

__io.read = function()
	return "1"
end

for k, v in pairs(__real__io) do
	if not __io[k] then
		__io[k] = v
	end
end

io = __io
function unpack(...)
	return table.unpack(...)
end

function os.execute(command)
	return __os_execute(command)
end

---@class OutputFileShim
---@field Path string
local OutputFileShim = {}

---@return OutputFileShim
function OutputFileShim.new(path)
	---@type OutputFileShim
	local self = {
		Path = path
	}

	setmetatable(self, OutputFileShim._mt)
	return self
end

function OutputFileShim:write(content)
	__file_write(self.Path, content)
end

function OutputFileShim:close()

end

OutputFileShim._mt = {
	__index = OutputFileShim
}

__real__io_open = io.open
function io.open(path, mode, ...)
	if mode:find("w") then
		return OutputFileShim.new(path)
	end

	return __real__io_open(path, mode, ...)
end

__real__print = print
function print(...)
	local notFound = true
	for _, v in pairs({ ... }) do
		local s = tostring(v)

		if s:find("AutocompleteMetadata") then
			zlib.enable()
			notFound = false
			break
		end

		if s:find("Decompressing") then
			notFound = false
		end
	end

	if notFound then
		zlib.disable()
	end

	return __real__print(...)
end