package net.brinkervii;

import net.brinkervii.rockit.fuel.cli.CLIStubs;
import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(
		name = "rockit-fuel",
		description = "Roblox API stub generator",
		subcommands = {CLIStubs.class})
public class RockitFuelCLI implements Runnable {
	public static void main(String[] arguments) {
		CommandLine.run(new RockitFuelCLI(), arguments);
	}

	@Override
	public void run() {
		CommandLine.usage(this, System.out);
	}
}
