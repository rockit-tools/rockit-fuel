package net.brinkervii.rockit.fuel.model.autocomplete;

import org.jsoup.nodes.Element;

public class AutocompleteItemStruct extends AutocompleteLibrary {
	public AutocompleteItemStruct(Element element) {
		super();

		super.processElement(element);
	}

	@Override
	public String toString() {
		return name;
	}
}
