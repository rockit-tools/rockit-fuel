package net.brinkervii.rockit.fuel.model.autocomplete;

import org.jsoup.nodes.Element;

public class AutocompleteCoreLibrary extends AutocompleteLibrary {
	public AutocompleteCoreLibrary(Element element) {
		super();

		super.processElement(element);
	}

	@Override
	public String toString() {
		return name;
	}
}
