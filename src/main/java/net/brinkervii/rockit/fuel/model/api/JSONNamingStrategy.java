package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;

public class JSONNamingStrategy extends PropertyNamingStrategy {
	@Override
	public String nameForField(MapperConfig<?> config, AnnotatedField field, String defaultName) {
		return field.getName();
	}

	@Override
	public String nameForGetterMethod(MapperConfig<?> config, AnnotatedMethod method, String defaultName) {
		return method.getName().substring(3);
	}

	@Override
	public String nameForSetterMethod(MapperConfig<?> config, AnnotatedMethod method, String defaultName) {
		return method.getName().substring(3);
	}
}
