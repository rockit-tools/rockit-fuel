package net.brinkervii.rockit.fuel.model.autocomplete;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.stream.Stream;

@Slf4j
public class RobloxAutocompleteDocument {
	private final ArrayList<AutocompleteLibrary> libraries = new ArrayList<>();
	private final ArrayList<String> reservedWords = new ArrayList<>();
	private final Document document;

	public RobloxAutocompleteDocument(Document document) {
		this.document = document;
		processDocument();
	}

	private void processDocument() {
		document.selectFirst("StudioAutocomplete").children().forEach(element -> {
			final String tagName = element.tagName();

			switch (tagName) {
				case "LuaLibrary":
					libraries.add(new AutocompleteLuaLibrary(element));
					break;

				case "ItemStruct":
					libraries.add(new AutocompleteItemStruct(element));
					break;

				case "CoreLibrary":
					libraries.add(new AutocompleteCoreLibrary(element));
					break;

				case "ReservedWords":
					element.children().stream()
							.filter(e -> e.hasAttr("name"))
							.map(e -> e.attr("name").trim())
							.forEach(this.reservedWords::add);
					break;

				default:
					log.warn("Unknown tag name " + tagName);
					break;
			}
		});
	}

	public Stream<AutocompleteLibrary> libraries() {
		return libraries.stream();
	}
}
