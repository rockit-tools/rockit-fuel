package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonSerialize
public class RobloxFunctionParameter {
	private String Default;
	private String Name;
	private RobloxType Type;
}
