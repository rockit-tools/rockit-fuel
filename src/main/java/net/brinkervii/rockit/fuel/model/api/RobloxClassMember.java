package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.ArrayList;

@Data
@JsonSerialize
public class RobloxClassMember {
	private String Category;
	private String MemberType;
	private String Name;
	//	private RobloxClassMemberSecurity Security;
	private ArrayList<String> Tags;
	private RobloxSerialization Serialization;
	private RobloxType ValueType;
	private ArrayList<RobloxFunctionParameter> Parameters;
	private RobloxType ReturnType;

	@JsonIgnore
	public boolean hasTags() {
		return Tags != null;
	}
}
