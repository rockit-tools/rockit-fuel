package net.brinkervii.rockit.fuel.model.autocomplete;

import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;

@Slf4j
public class AutocompleteType {
	private final static HashSet<String> UNIQUE_TYPES = new HashSet<>();
	private final static String[] VANILLA_TYPES = new String[]{
			"number",
			"string",
			"nil",
			"boolean",
			"function",
			"table",
			"thread"
	};
	private final String s;
	private final boolean vanilla;

	private AutocompleteType(String s) {
		this.s = s;
		this.vanilla = inVanillaArray(s);
	}

	private static boolean inVanillaArray(String s) {
		for (String vanillaType : VANILLA_TYPES) {
			if (vanillaType.equalsIgnoreCase(s)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public String toString() {
		return s;
	}

	public static AutocompleteType fromString(String s) {
		String type;

		switch (s.toLowerCase()) {
			case "dictionary":
				type = "table";
				break;

			case "bool":
				type = "boolean";
				break;

			case "null":
				type = "nil";
				break;

			case "variant":
			case "value":
				type = "any";
				break;

			case "iterator":
				type = "function";
				break;

			default:
				type = s;
				break;
		}

		final AutocompleteType autocompleteType = new AutocompleteType(type);
		if (!autocompleteType.isVanilla() && UNIQUE_TYPES.add(type)) {
			log.info("Discovered new type " + type);
		}

		return autocompleteType;
	}

	private boolean isVanilla() {
		return vanilla;
	}
}
