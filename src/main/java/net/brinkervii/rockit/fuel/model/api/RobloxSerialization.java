package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonSerialize
public class RobloxSerialization {
	private boolean CanLoad;
	private boolean CanSave;
}
