package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RobloxObjectMapper extends ObjectMapper {
	public RobloxObjectMapper() {
		setPropertyNamingStrategy(new JSONNamingStrategy());
		disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}
}
