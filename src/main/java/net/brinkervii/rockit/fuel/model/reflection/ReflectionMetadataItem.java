package net.brinkervii.rockit.fuel.model.reflection;

import java.util.HashMap;

public class ReflectionMetadataItem {
	private HashMap<String, String> properties = new HashMap<>();

	public void putProperty(String key, String value) {
		final String lowerKey = key.toLowerCase();
		if (properties.containsKey(lowerKey)) return;

		properties.put(lowerKey, value);
	}

	protected String getProperty(String key) {
		final String lowercaseKey = key.toLowerCase();
		if (properties.containsKey(lowercaseKey)) return properties.get(lowercaseKey);

		return "";
	}

	protected boolean getBooleanProperty(String key) {
		return "true".equalsIgnoreCase(getProperty(key));
	}

	protected int getIntegerProperty(String key) {
		return Integer.parseInt(getProperty(key));
	}

	@Override
	public int hashCode() {
		return properties.hashCode();
	}
}
