package net.brinkervii.rockit.fuel.model.reflection;

import lombok.Getter;

import java.util.ArrayList;

public class ReflectionMetadataClass extends ReflectionMetadataItem {
	@Getter
	private ArrayList<ReflectionMetadataMember> members = new ArrayList<>();

	public String name() {
		return getProperty("Name");
	}

	public String classCategory() {
		return getProperty("ClassCategory");
	}

	public String summary() {
		return getProperty("summary");
	}

	public int explorerOrder() {
		return getIntegerProperty("ExplorerOrder");
	}

	public int explorerImageIndex() {
		return getIntegerProperty("ExplorerImageIndex");
	}

	public String preferredParent() {
		return getProperty("PreferredParent");
	}

	public String preferredParents() {
		return getProperty("PreferredParents");
	}

	public boolean browseable() {
		return getBooleanProperty("Browsable");
	}

	@Override
	public String toString() {
		return name();
	}
}
