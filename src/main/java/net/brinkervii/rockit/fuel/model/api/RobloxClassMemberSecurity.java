package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonSerialize
public class RobloxClassMemberSecurity {
	private String Read;
	private String Write;
}
