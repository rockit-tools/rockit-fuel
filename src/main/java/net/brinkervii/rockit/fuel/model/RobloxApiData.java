package net.brinkervii.rockit.fuel.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.fuel.model.api.RobloxAPI;
import net.brinkervii.rockit.fuel.model.api.RobloxObjectMapper;
import net.brinkervii.rockit.fuel.model.autocomplete.RobloxAutocompleteDocument;
import net.brinkervii.rockit.fuel.model.reflection.ReflectionMetadata;
import net.brinkervii.rockit.fuel.rbxautocomplete.RobloxAutocompleteData;
import net.brinkervii.rockit.fuel.request.*;
import okhttp3.OkHttpClient;

import java.io.File;
import java.io.IOException;

@Data
@Slf4j
public class RobloxApiData {
	private final RobloxAPI robloxAPI;
	private final ReflectionMetadata reflectionMetadata;
	private final RobloxAutocompleteDocument autocompleteDocument;

	private RobloxApiData(RobloxAPI robloxAPI, ReflectionMetadata reflectionMetadata, RobloxAutocompleteDocument autocompleteDocument) {
		this.robloxAPI = robloxAPI;
		this.reflectionMetadata = reflectionMetadata;
		this.autocompleteDocument = autocompleteDocument;
	}

	public static RobloxApiData download(OkHttpClient httpClient) throws ApiDownloadException {
		final String versionString = getVersionString(httpClient);
		final ReflectionMetadata reflectionMetadata = getReflectionMetadata(httpClient, versionString);
		final RobloxAPI robloxAPI = getRobloxAPI(httpClient, versionString);

		final File robloxStudioBinaryFile = getRobloxStudioBinaryFile(httpClient, versionString);
		final RobloxAutocompleteData autocompleteData = new RobloxAutocompleteData(versionString, robloxStudioBinaryFile);
		autocompleteData.run();

		if (!autocompleteData.hasAutocompleteDocument())
			throw new ApiDownloadException("Failed to get the Autocomplete document");

		final RobloxAutocompleteDocument autocompleteDocument = new RobloxAutocompleteDocument(autocompleteData.getAutocompleteDocument());

		log.info("Assembled API data");
		return new RobloxApiData(robloxAPI, reflectionMetadata, autocompleteDocument);
	}

	private static RobloxAPI getRobloxAPI(OkHttpClient httpClient, String versionString) throws ApiDownloadException {
		ApiDumpRequest apiDumpRequest = new ApiDumpRequest(httpClient)
				.branch(RobloxBranch.getDefault())
				.versionId(versionString);
		try {
			final String apiDump = apiDumpRequest.get();
			final ObjectMapper mapper = new RobloxObjectMapper();
			return mapper.readValue(apiDump, RobloxAPI.class);
		} catch (InvalidRequestException | IOException e) {
			throw new ApiDownloadException(e);
		}
	}

	private static ReflectionMetadata getReflectionMetadata(OkHttpClient httpClient, String versionString) throws ApiDownloadException {
		final ReflectionMetadataRequest mdRequest = new ReflectionMetadataRequest(httpClient)
				.branch(RobloxBranch.getDefault())
				.versionId(versionString);

		try {
			mdRequest.download();
			return new ReflectionMetadata(mdRequest.get()).translate();
		} catch (InvalidRequestException e) {
			throw new ApiDownloadException(e);
		}
	}

	private static File getRobloxStudioBinaryFile(OkHttpClient httpClient, String versionString) throws ApiDownloadException {
		final RobloxStudioBinaryRequest request = new RobloxStudioBinaryRequest(httpClient)
				.branch(RobloxBranch.getDefault())
				.versionId(versionString);

		try {
			request.download();
			return request.getBinaryFile();
		} catch (InvalidRequestException e) {
			throw new ApiDownloadException(e);
		}
	}

	private static String getVersionString(OkHttpClient httpClient) throws ApiDownloadException {
		final VersionRequest versionRequest = new VersionRequest(httpClient)
				.branch(RobloxBranch.getDefault())
				.versionType(RobloxVersionType.VersionQtStudio)
				.binaryType(RobloxBinaryType.WindowsStudio);

		try {
			return versionRequest.getVersion();
		} catch (InvalidRequestException e) {
			throw new ApiDownloadException(e);
		}
	}
}
