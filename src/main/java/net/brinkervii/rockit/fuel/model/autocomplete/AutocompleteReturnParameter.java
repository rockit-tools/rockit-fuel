package net.brinkervii.rockit.fuel.model.autocomplete;

import org.jsoup.nodes.Element;

public class AutocompleteReturnParameter extends AutocompleteTypedVariable {
	public AutocompleteReturnParameter(Element element) {
		super(element);
	}
}
