package net.brinkervii.rockit.fuel.rbxautocomplete;


import lombok.Getter;
import net.brinkervii.rockit.fuel.util.FuelCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RobloxAutocompleteData implements Runnable {
	private final static FuelCache CACHE = new FuelCache(RobloxAutocompleteData.class.getSimpleName());
	private final String XML_FILENAME = "AutocompleteMetadata.xml";

	private final String versionString;
	private final File studioBinaryFile;
	@Getter
	private Document autocompleteDocument = null;

	public RobloxAutocompleteData(String versionString, File studioBinaryFile) {
		this.versionString = versionString;
		this.studioBinaryFile = studioBinaryFile;
	}

	private String cacheKey() {
		return versionString + "-" + XML_FILENAME;
	}

	@Override
	public void run() {
		final String cacheKey = cacheKey();
		if (CACHE.contains(cacheKey)) {
			try {
				useAutoCompleteFile(CACHE.getString(cacheKey));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			final QtExtract extractor = new QtExtract(studioBinaryFile);
			extractor.setWriteCallback(this::onFileExtraction);

			try {
				extractor.extract();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void onFileExtraction(String path, byte[] contents) {
		if (!path.contains(XML_FILENAME))
			return;

		if (!path.contains("zlib")) {
			useAutoCompleteFile(new String(contents));
		}
	}

	private void useAutoCompleteFile(String s) {
		final String cacheKey = cacheKey();
		if (!CACHE.contains(cacheKey)) {
			try {
				CACHE.store(cacheKey, s.getBytes(StandardCharsets.UTF_8));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		this.autocompleteDocument = Jsoup.parse(s, "", Parser.xmlParser());
	}

	public boolean hasAutocompleteDocument() {
		return autocompleteDocument != null;
	}
}
