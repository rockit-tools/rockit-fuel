package net.brinkervii.rockit.fuel.rbxautocomplete;

import lombok.Getter;
import lombok.Setter;
import org.luaj.vm2.*;
import org.luaj.vm2.lib.jse.JsePlatform;
import org.luaj.vm2.luajc.LuaJC;

import java.io.*;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.BiConsumer;

public class QtExtract {
	private final static String RES_PREFIX = "com/github/axstin/qtextract/";
	private final Path path;
	@Getter
	private final Globals globals;
	@Getter
	private final QtExtractArguments arguments;

	@Setter
	private BiConsumer<String, byte[]> writeCallback = null;

	public QtExtract(File file) {
		this.path = file.toPath().toAbsolutePath().normalize();
		this.arguments = new QtExtractArguments(file);
		this.globals = JsePlatform.standardGlobals();

		LuaJC.install(globals);

		globals.set("require", new LuaRequireShim(globals, RES_PREFIX));
		globals.set("__io", new LuaIOShim(this));
		globals.set("arg", arguments);

		globals.set("__os_execute", new LuaFunction() {
			@Override
			public Varargs invoke(Varargs args) {
				return args;
			}
		});

		globals.set("__file_write", new LuaFunction() {
			@Override
			public LuaValue call(LuaValue arg1, LuaValue arg2) {
				if (arg1 instanceof LuaString && arg2 instanceof LuaString) {
					write(new String(((LuaString) arg1).m_bytes), ((LuaString) arg2).m_bytes);
				}

				return arg1;
			}
		});
	}

	private void write(String path, byte[] content) {
		if (writeCallback != null) writeCallback.accept(path, content);
	}

	public void extract() throws IOException {
		final InputStream prepare = getClass().getClassLoader().getResourceAsStream("net/brinkervii/fuel-qt-prepare.lua");
		globals.load(new InputStreamReader(Objects.requireNonNull(prepare)), "fuel-qt-prepare.lua").call();

		final String qtExtractScriptPath = RES_PREFIX + "qtextract.lua";
		final InputStream qtExtractScript = getClass().getClassLoader().getResourceAsStream(qtExtractScriptPath);
		if (qtExtractScript == null)
			throw new FileNotFoundException(qtExtractScriptPath);

		final LuaValue script = globals.load(new InputStreamReader(qtExtractScript), "qtextract.lua");

		script.call();
	}
}
