package net.brinkervii.rockit.fuel.rbxautocomplete;

import org.luaj.vm2.*;
import org.luaj.vm2.lib.jse.LuajavaLib;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

public class LuaZLib extends LuaTable {
	private boolean enabled = false;

	public LuaZLib() {
		set("decompress", new LuaFunction() {
			@Override
			public Varargs invoke(Varargs args) {
				return LuajavaLib.varargsOf(new LuaValue[]{LuajavaLib.valueOf(decompress(args.arg(1)))});
			}
		});

		set("enable", new LuaFunction() {
			@Override
			public LuaValue call() {
				enabled = true;
				return LuaValue.valueOf(true);
			}
		});

		set("disable", new LuaFunction() {
			@Override
			public LuaValue call() {
				enabled = false;
				return LuaValue.valueOf(true);
			}
		});
	}

	private byte[] decompress(LuaValue arg) {
		if (arg instanceof LuaString) {
			final LuaString str = (LuaString) arg;
			return decompress(str.m_bytes, str.m_length, str.m_offset);
		}

		return new byte[0];
	}

	private byte[] decompress(byte[] m_bytes, int m_length, int m_offset) {
		if (!enabled) {
			return new byte[0];
		}

		final InflaterInputStream inflaterInputStream = new InflaterInputStream(new ByteArrayInputStream(m_bytes, m_offset, m_length));
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			byte[] buffer = new byte[1024];
			int len;

			while ((len = inflaterInputStream.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bos.toByteArray();
	}
}
