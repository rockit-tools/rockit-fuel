package net.brinkervii.rockit.fuel.rbxautocomplete;

import lombok.Getter;
import org.luaj.vm2.LuaTable;

import java.io.File;

public class QtExtractArguments extends LuaTable {
	@Getter
	private final String outputDirectory = "qt-extract-output";

	public QtExtractArguments(File file) {
		set(1, file.toString());
		set(2, "--output");
		set(3, outputDirectory);
	}
}
