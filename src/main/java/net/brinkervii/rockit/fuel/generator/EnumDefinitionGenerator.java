package net.brinkervii.rockit.fuel.generator;

import lombok.Getter;
import net.brinkervii.rockit.fuel.model.RobloxApiData;

public class EnumDefinitionGenerator implements DefinitionGenerator {
	@Getter
	private String luaString = "";
	private final RobloxApiData data;

	public EnumDefinitionGenerator(RobloxApiData data) {
		this.data = data;
	}

	private String lineSeparator() {
		return System.lineSeparator();
	}

	@Override
	public void generate() {
		final StringBuilder stringBuilder = new StringBuilder();

		stringBuilder
				.append("---@class EnumItem")
				.append(lineSeparator())
				.append("---@field Value number")
				.append(lineSeparator())
				.append("---@field Name string")
				.append(lineSeparator())
				.append("---@field EnumType")
				.append(lineSeparator())
				.append(lineSeparator())
				.append("Enum = {}")
				.append(lineSeparator())
				.append(lineSeparator());

		data.getRobloxAPI().getEnums().forEach(anEnum -> {
			stringBuilder
					.append("---@class Enum.")
					.append(anEnum.getName())
					.append(lineSeparator())
					.append("Enum.")
					.append(anEnum.getName())
					.append(" = {")
					.append(lineSeparator());

			anEnum.getItems().forEach(enumItem -> {
				stringBuilder
						.append("\t---@type EnumItem")
						.append(lineSeparator())
						.append("\t")
						.append(enumItem.getName())
						.append(" = ")
						.append(enumItem.getValue())
						.append(",")
						.append(lineSeparator());
			});

			stringBuilder
					.append("}")
					.append(lineSeparator())
					.append(lineSeparator());
		});

		this.luaString = stringBuilder.toString();
	}
}
