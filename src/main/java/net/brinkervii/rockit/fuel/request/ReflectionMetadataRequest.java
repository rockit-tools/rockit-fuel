package net.brinkervii.rockit.fuel.request;

import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.nio.file.Files;

@Slf4j
public class ReflectionMetadataRequest extends RobloxStudioArchiveRequest<ReflectionMetadataRequest> {
	private final static String FILE_NAME = "ReflectionMetadata.xml";
	private String metadataString = null;

	public ReflectionMetadataRequest(OkHttpClient client) {
		super(client);
		this.self = this;
	}

	public void download() throws InvalidRequestException {
		if (metadataString != null) return;

		super.download();

		zip.files()
				.filter(file -> file.getName().equalsIgnoreCase(FILE_NAME))
				.forEach(file -> {
					try {
						this.metadataString = new String(Files.readAllBytes(file.toPath()));
					} catch (IOException e) {
						e.printStackTrace();
					}
				});

		if (metadataString == null)
			throw new InvalidRequestException("Failed to locate ReflectionMetaData");
	}

	public String get() {
		return metadataString;
	}
}
