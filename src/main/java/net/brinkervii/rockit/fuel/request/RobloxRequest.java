package net.brinkervii.rockit.fuel.request;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

public abstract class RobloxRequest<T extends RobloxRequest> {
	protected final OkHttpClient client;
	protected RobloxBranch branch = null;
	protected String versionId = null;
	protected T self = null;

	protected RobloxRequest(OkHttpClient client) {
		this.client = client;
	}

	public T branch(RobloxBranch branch) {
		this.branch = branch;
		return self;
	}

	public T versionId(String versionId) {
		this.versionId = versionId;
		return self;
	}

	protected String setupUrl() throws InvalidRequestException {
		if (branch == null)
			throw new InvalidRequestException("Version request is missing the branch parameter");
		if (versionId == null)
			throw new InvalidRequestException("Version request is missing the versionId parameter");

		return String.format("https://s3.amazonaws.com/setup.%s.com/%s", branch.getBranchName(), versionId);
	}

	protected static String responseToString(Response response) throws InvalidRequestException, IOException {
		final ResponseBody body = response.body();
		if (body == null)
			throw new InvalidRequestException("Failed to retrieve a response");

		return new String(body.bytes());
	}
}
