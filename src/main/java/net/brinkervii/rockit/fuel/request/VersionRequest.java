package net.brinkervii.rockit.fuel.request;

import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

@Slf4j
public class VersionRequest extends RobloxRequest<VersionRequest> {
	private RobloxBinaryType binaryType = null;
	private RobloxVersionType versionType = null;

	public VersionRequest(OkHttpClient client) {
		super(client);
		this.self = this;
	}

	public VersionRequest versionType(RobloxVersionType versionType) {
		this.versionType = versionType;
		return this;
	}

	public VersionRequest binaryType(RobloxBinaryType binaryType) {
		this.binaryType = binaryType;
		return this;
	}

	public String getVersion() throws InvalidRequestException {
		if (branch == null)
			throw new InvalidRequestException("Version request is missing the branch parameter");
		if (versionType == null)
			throw new InvalidRequestException("Version request is missing the versionType parameter");

		final String versionUrl = String.format(
				"https://s3.amazonaws.com/setup.%s.com/%s",
				branch.getBranchName(),
				versionType.getUrlParam()
		);

		log.info("Downloading Roblox (DEPLOYED) version string: " + versionUrl);

		final Request request = new Request.Builder()
				.url(versionUrl)
				.build();

		try {
			final Response response = client.newCall(request).execute();
			return responseToString(response);
		} catch (IOException e) {
			throw new InvalidRequestException(e);
		}
	}
}
