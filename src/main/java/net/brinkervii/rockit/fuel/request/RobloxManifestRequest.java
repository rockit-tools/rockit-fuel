package net.brinkervii.rockit.fuel.request;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class RobloxManifestRequest extends RobloxRequest<RobloxManifestRequest> {
	public RobloxManifestRequest(OkHttpClient client) {
		super(client);
		this.self = this;
	}

	public String get() throws InvalidRequestException {
		final String url = setupUrl() + "-rbxManifest.txt";

		final Request request = new Request.Builder()
				.url(url)
				.build();

		try {
			final Response response = client.newCall(request).execute();
			return responseToString(response);
		} catch (IOException e) {
			throw new InvalidRequestException(e);
		}
	}
}
