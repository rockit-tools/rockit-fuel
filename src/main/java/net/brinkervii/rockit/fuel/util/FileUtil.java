package net.brinkervii.rockit.fuel.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;

@Slf4j
public class FileUtil {
	private final static File TEMP;
	private final static LinkedList<File> tempDirectories = new LinkedList<>();

	static {
		switch (System.getProperty("os.name")) {
			case "Linux":
				TEMP = new File("/tmp");
				break;

			default:
				TEMP = new File(String.format(".%s.tmp", File.separator));
		}

		if (!TEMP.exists()) {
			TEMP.mkdirs();
		}

		Runtime.getRuntime().addShutdownHook(new Thread(() -> tempDirectories.forEach(file -> {
			try {
				boolean success = Files.walk(file.toPath())
						.filter(path -> !Files.isSymbolicLink(path))
						.map(path -> path.toFile().delete())
						.reduce(true, (a, b) -> a && b) && file.delete();

				log.info(String.valueOf(success));
			} catch (IOException e) {
				e.printStackTrace();
			}
		})));
	}

	public static File getTemp(String purpose) throws IOException {
		final File file = new File(TEMP, purpose);
		if (!file.exists()) {
			if (!file.mkdirs()) {
				throw new IOException("Could not create temporary directory " + file);
			}
		}

		tempDirectories.add(file);

		return file;
	}
}
