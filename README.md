# Rockit Fuel
Rockit Fuel is a tool for working with the Roblox API files provided by Roblox. A tool like Rockit Fuel is required
since Roblox has no real 'official' way for game developers to get a hold of these files. These files also come
in various complex formats.

## EmmyLua stub generation
Currently, Rockit Fuel's main focus is generating autocompletion stubs for the IntelliJ EmmyLua plugin. These can
be generated with the Rockit Fuel CLI:
```bash
rockit-fuel stubs [-f output_path]
``` 
**Sidenote**: When the output path is not provided, Rockit Fuel will generate a `dist` folder with the autocompletion
stubs in the `Roblox.zip` file.

### Stubs file format
The stubs are packed into a `zip` archive. This archive may be added as a library to an EmmyLua project. When this has
been done, EmmyLua will autocomplete for the Roblox API.

## Dependency management and deployment
This project is managed using Apache Maven. The build file is configured to create both a regular jar and a fat jar.